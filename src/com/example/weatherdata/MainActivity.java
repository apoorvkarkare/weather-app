package com.example.weatherdata;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends Activity {
	private static final String url = "http://api.openweathermap.org/data/2.5/weather?q=";
	EditText cityName;
	TextView longitude, latitude, sunrise, sunset, temperature, pressure,
			humidity;
	Button getButton;
	ImageView image;
	Bitmap bitmap;
	String show_image;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		cityName = (EditText) findViewById(R.id.city);
		longitude = (TextView) findViewById(R.id.longitude);
		latitude = (TextView) findViewById(R.id.latitude);
		sunrise = (TextView) findViewById(R.id.sunrise);
		sunset = (TextView) findViewById(R.id.sunset);
		temperature = (TextView) findViewById(R.id.temperature);
		pressure = (TextView) findViewById(R.id.pressure);
		humidity = (TextView) findViewById(R.id.humidity);
		image = (ImageView) findViewById(R.id.image);
		getButton = (Button) findViewById(R.id.button);

		getButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				isNetworkAvailable(v);
			}
		});
	}

	public void isNetworkAvailable(View view) {
		// Gets the URL from the UI's text field.
		String stringUrl = url + cityName.getText().toString();
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected()) {
			new DownloadDataTask().execute(stringUrl);
			new getImageTask().execute();
		} else {
			System.out.println("no net");

		}
	}

	private class DownloadDataTask extends AsyncTask<String, Void, String[]> {

		@Override
		protected String[] doInBackground(String... urls) {

			// params comes from the execute() call: params[0] is the url.
			try {
				String data = getJsonDataFromUrl(urls[0]);
				JSONObject jo = new JSONObject(data);

				JSONObject cord = jo.getJSONObject("coord");
				JSONObject sys = jo.getJSONObject("sys");
				JSONObject main = jo.getJSONObject("main");
				JSONArray image_array = jo.getJSONArray("weather");
				JSONObject image = image_array.getJSONObject(0);
				show_image = image.getString("icon");

				String result[] = { cord.getString("lon"),
						cord.getString("lat"), sys.getString("sunrise"),
						sys.getString("sunset"), main.getString("temp"),
						main.getString("pressure"), main.getString("humidity") };

				return result;
			} catch (IOException | JSONException e) {
				String a[] = {};
				return a;
			}
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result[]) {

			longitude.setText(result[0]);
			latitude.setText(result[1]);
			String t = convetTime(result[2]);
			String t1 = convetTime(result[3]);
			sunrise.setText(t);
			sunset.setText(t1);
			temperature.setText(result[4]);
			pressure.setText(result[5]);
			humidity.setText(result[6]);
			

		}

		private String convetTime(String time) {
			// TODO Auto-generated method stub
			long dv = Long.valueOf(time) * 1000;
			java.util.Date df = new java.util.Date(dv);
			String vv = new SimpleDateFormat("hh:mma").format(df);
			//System.out.println(localTime);
			Calendar mCalendar = new GregorianCalendar();  
			TimeZone mTimeZone = mCalendar.getTimeZone();  
			int mGMTOffset = mTimeZone.getRawOffset();  
			vv= ("GMT+"+TimeUnit.HOURS.convert(mGMTOffset, TimeUnit.MILLISECONDS)); 
			return vv;
		}
		
		
	}

	private String getJsonDataFromUrl(String myurl) throws IOException {
		InputStream is = null;
		// Only display the first 500 characters of the retrieved
		// web page content.
		int len = 500;

		try {
			URL url = new URL(myurl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setReadTimeout(10000 /* milliseconds */);
			conn.setConnectTimeout(15000 /* milliseconds */);
			conn.setRequestMethod("GET");
			conn.setDoInput(true);
			// Starts the query
			conn.connect();
			int response = conn.getResponseCode();
			Log.d("hello ", "The response is: " + response);
			is = conn.getInputStream();

			// Convert the InputStream into a string
			String contentAsString = readIt(is, len);
			// bitmap = BitmapFactory.decodeStream(is);
			return contentAsString;

			// Makes sure that the InputStream is closed after the app is
			// finished using it.
		} finally {
			if (is != null) {
				is.close();
			}
		}
	}

	public String readIt(InputStream stream, int len) throws IOException,
			UnsupportedEncodingException {
		Reader reader = null;
		reader = new InputStreamReader(stream, "UTF-8");
		char[] buffer = new char[len];
		reader.read(buffer);
		return new String(buffer);
	}

	private class getImageTask extends AsyncTask<String, Void, Bitmap> {

		@Override
		protected Bitmap doInBackground(String... params) {
			// TODO Auto-generated method stub
			try {
				bitmap = getImageFromUrl("http://openweathermap.org/img/w/"
						+ show_image + ".png");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return bitmap;
		}
		
		protected void onPostExecute(Bitmap bitmap){
			image.setImageBitmap(bitmap);
			
		}
			
	

	public Bitmap getImageFromUrl(String myurl) throws IOException {
		InputStream is = null;
		// Only display the first 500 characters of the retrieved
		// web page content.

		try {
			URL url = new URL(myurl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setReadTimeout(10000 /* milliseconds */);
			conn.setConnectTimeout(15000 /* milliseconds */);
			conn.setRequestMethod("GET");
			conn.setDoInput(true);
			// Starts the query
			conn.connect();
			int response = conn.getResponseCode();
			Log.d("hello ", "The response is: " + response);
			is = conn.getInputStream();

			System.out.println("i am in image");

			Bitmap bitmap = BitmapFactory.decodeStream(is);
			return bitmap;
		} finally {
			if (is != null) {
				is.close();
			}
		}
	}

	

}}
